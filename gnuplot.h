#include <map>

HWND HWND_VIEW; // EmEditor のハンドル

const WCHAR SZ_CONFIG[]                    = L"Gnuplot";
const WCHAR SZ_VALUE_WGNUPLOT_EXEC_PATH[]  = L"WGNUPLOT_EXEC_PATH";
const WCHAR SZ_VALUE_SHORTCUT_KEY_STRING[] = L"SHORTCUT_KEY_STRING";
const WCHAR SZ_VALUE_WAIT_FOR_START_MSEC[] = L"WAIT_FOR_START_MSEC";

const WCHAR SZ_DEFAULT_WGNUPLOT_EXEC_PATH[]  = L"wgnuplot.exe";
const WCHAR SZ_DEFAULT_SHORTCUT_KEY_STRING[] = L"C+S+q";
const DWORD DW_DEFAULT_WAIT_FOR_START_MSEC   = 0;

const size_t MAX_SIZE_OF_SHORTCUT_KEY_STRING  = 8;
const size_t MAX_DIGIT_OF_WAIT_FOR_START_MSEC = 4; // この桁数の数字までを扱う
const size_t MAX_SIZE_OF_WAIT_FOR_START_MSEC  = MAX_DIGIT_OF_WAIT_FOR_START_MSEC + 1; // 終端文字 1 文字分を追加


int iShortcutKyeFlags;  // ショートカットキーの装飾キービットフラグ
WCHAR wcShortcutKey;    // ショートカットキーのキーコード


inline bool DWORD2LPWSTR(DWORD dwInt, WCHAR * szStr, size_t sz){
  int res = swprintf_s(szStr, sz, L"%d", dwInt);
  return res != 0;
}

inline bool LPWSTR2DWORD(WCHAR * szStr, DWORD & dwInt){
  int res = swscanf_s(szStr, L"%d", &dwInt);
  return (res != EOF && res != 0);
}

/// <summary>
/// Alt, Ctrl, Shift の状態から，int のビットフラグを計算
/// </summary>
inline int calcShortcutKeyFlags(bool flagAlt, bool flagCtl, bool flagSft){
  return (flagAlt ? 1 : 0) + (flagCtl ? 2 : 0) + (flagSft ? 4 : 0);
}

/// <summary>
/// GetKeyState から Alt, Ctrl, Shift の状態を取得し，ビットフラグで返す
/// </summary>
inline int getShortcutKeyFlagsFromKeyState(){
  bool flagAlt = (GetKeyState(VK_MENU   ) < 0);
  bool flagCtl = (GetKeyState(VK_CONTROL) < 0);
  bool flagSft = (GetKeyState(VK_SHIFT  ) < 0);
  return calcShortcutKeyFlags(flagAlt, flagCtl, flagSft);
}

/// <summary>
/// szShortcutKeyString から装飾キーのフラグとショートカットキーを読み取り，
/// それぞれビットフラグ iShortcutKeyFlags と文字コード wcShortcutKey に格納
/// szShortcutKeyString のパースに成功したかどうかを bool で返す
/// </summary>
inline bool setShortcutKey(WCHAR * szShortcutKeyString){
  bool flagAlt = false;
  bool flagCtl = false;
  bool flagSft = false;
  WCHAR k = NULL;
  WCHAR * c = szShortcutKeyString;
  while(*c){
    if(*c == L'A') flagAlt = true;
    if(*c == L'C') flagCtl = true;
    if(*c == L'S') flagSft = true;
    if(L'a' <= *c && *c <= L'z') k = *c - 0x20; // 大文字にする
    if(L'0' <= *c && *c <= L'9') k = *c;
    ++c;
  }
  iShortcutKyeFlags = calcShortcutKeyFlags(flagAlt, flagCtl, flagSft);
  wcShortcutKey = k;
  return wcShortcutKey != NULL;
}


/// <summary>
/// wgnuplot の HWND を取得するためのコールバック関数
/// lParam には HWND の配列へのポインタを指定する．
/// 配列の 0 番目にウィンドウクラス wgnuplot_parent の HWND が，
/// 配列の 1 番目にウィンドウクラス wgnuplot_text の HWND が格納される
/// </summary>
BOOL CALLBACK callbackGetGnuplotHWND(HWND  hwnd, LPARAM  lParam){
  WCHAR * TEXTCLASS = L"wgnuplot_text";
  HWND * tmpHWNDArray = (HWND *)lParam;
  tmpHWNDArray[0] = hwnd;
  tmpHWNDArray[1] = FindWindowEx(hwnd, NULL, TEXTCLASS, NULL);

  return (tmpHWNDArray[1] == NULL);
}

/// <summary>
/// フルパスからディレクトリ名を返す
/// szFullPath : フルパス
/// szDir      : ディレクトリ名
/// sz         : szDir の最大文字数（バイト数ではない）
/// szFullPath = "c:\\abc\\def\\ghi.ext" -> szDir = "c:\\abc\\def\\"
/// </summary>
inline void getDirectoryName(const WCHAR * szFullPath, WCHAR * szDir, size_t sz){
  WCHAR szDrive[8], szDirWithoutDrive[MAX_PATH], szFileName[MAX_PATH], szExt[8];
  _wsplitpath_s(szFullPath, szDrive           , _countof(szDrive),
                            szDirWithoutDrive , _countof(szDirWithoutDrive),
                            szFileName        , _countof(szFileName),
                            szExt             , _countof(szExt));
  wcscpy_s (szDir, sz, szDrive);
  wcsncat_s(szDir, sz, szDirWithoutDrive, _TRUNCATE);
}


/// <summary>
/// EmEditor に保存されている文字列を読み込む
/// hwndView      : EmEditor のウィンドウハンドル
/// szValueString : 情報識別変数（レジストリのキー）
/// szStr         : 読み込んだ値が格納される
/// szDefaultStr  : 保存されていなかった場合に szStr に格納されるデフォルト文字列
/// </summary>
inline bool loadStringFromEmEditor(HWND hwndView, const WCHAR * szValueString, WCHAR * szStr, size_t sz, const WCHAR * szDefaultStr){
  DWORD dSize = static_cast< DWORD >(sz * sizeof(WCHAR));
  if(ERROR_SUCCESS == Editor_RegQueryValue(hwndView, EEREG_EMEDITORPLUGIN, SZ_CONFIG, szValueString, REG_SZ, (BYTE *)szStr, &dSize, 0)){
    return true;
  }else{
    wcscpy_s(szStr, sz, szDefaultStr);
    return false;
  }
}

/// <summary>
/// EmEditor に保存されている整数値(DWORD)を読み込む
/// hwndView      : EmEditor のウィンドウハンドル
/// szValueString : 情報識別変数（レジストリのキー）
/// dwpInt        : 読み込んだ値が格納される
/// dwDefaultInt  : 保存されていなかった場合に dwpInt に格納されるデフォルト値
/// </summary>
inline bool loadIntegerFromEmEditor(HWND hwndView, const WCHAR * szValueString, DWORD * dwpInt, const DWORD dwDefaultInt){
  DWORD dSize = sizeof(DWORD);
  if(ERROR_SUCCESS == Editor_RegQueryValue(hwndView, EEREG_EMEDITORPLUGIN, SZ_CONFIG, szValueString, REG_DWORD, (BYTE *)dwpInt, &dSize, 0)){
    return true;
  }else{
    *dwpInt = dwDefaultInt;
    return false;
  }
}

inline bool loadWgnuplotExecPath(HWND hwndView, WCHAR * szWgnuplotExecPath){
  return loadStringFromEmEditor(hwndView, SZ_VALUE_WGNUPLOT_EXEC_PATH, szWgnuplotExecPath, MAX_PATH, SZ_DEFAULT_WGNUPLOT_EXEC_PATH);
}

inline bool loadShortcutKeyString(HWND hwndView, WCHAR * szShortcutKeyString){
  return loadStringFromEmEditor(hwndView, SZ_VALUE_SHORTCUT_KEY_STRING, szShortcutKeyString, MAX_SIZE_OF_SHORTCUT_KEY_STRING, SZ_DEFAULT_SHORTCUT_KEY_STRING);
}

inline bool loadWaitForStartmSec(HWND hwndView, DWORD * dwpWaitForStartmSec){
  return loadIntegerFromEmEditor(hwndView, SZ_VALUE_WAIT_FOR_START_MSEC, dwpWaitForStartmSec, DW_DEFAULT_WAIT_FOR_START_MSEC);
}

inline bool loadWaitForStartmSec(HWND hwndView, WCHAR * szWaitForStartmSec){
  DWORD dwWaitForStartmSec;
  bool res = loadWaitForStartmSec(hwndView, &dwWaitForStartmSec);
  DWORD2LPWSTR(dwWaitForStartmSec, szWaitForStartmSec, MAX_SIZE_OF_WAIT_FOR_START_MSEC);
  return res;
}


// プロパティ設定用ダイアログプロシージャ
INT_PTR CALLBACK DlgProc(HWND hDlg, UINT msg, WPARAM wParam, LPARAM /* lParam */){
  WCHAR szWgnuplotExecPath[MAX_PATH];
  WCHAR szShortcutKeyString[MAX_SIZE_OF_SHORTCUT_KEY_STRING];
  DWORD dwWaitForStartmSec;
  WCHAR szWaitForStartmSec[MAX_SIZE_OF_WAIT_FOR_START_MSEC];

  switch(msg){
	case WM_INITDIALOG:
    loadWgnuplotExecPath(HWND_VIEW, szWgnuplotExecPath);
    SetDlgItemText(hDlg, IDC_EDIT1, szWgnuplotExecPath);

    loadShortcutKeyString(HWND_VIEW, szShortcutKeyString);
    SetDlgItemText(hDlg, IDC_EDIT2, szShortcutKeyString);

    loadWaitForStartmSec(HWND_VIEW, szWaitForStartmSec);
    SetDlgItemText(hDlg, IDC_EDIT3, szWaitForStartmSec);

    return FALSE;
	case WM_COMMAND:
    switch(wParam){
    case IDOK:
      GetDlgItemText(hDlg, IDC_EDIT1, szWgnuplotExecPath, MAX_PATH);
      Editor_RegSetValue(HWND_VIEW, EEREG_EMEDITORPLUGIN, SZ_CONFIG, SZ_VALUE_WGNUPLOT_EXEC_PATH, REG_SZ, (const BYTE *)szWgnuplotExecPath, MAX_PATH * sizeof(WCHAR), 0);

      GetDlgItemText(hDlg, IDC_EDIT2, szShortcutKeyString, MAX_SIZE_OF_SHORTCUT_KEY_STRING);
      if(setShortcutKey(szShortcutKeyString)){
        Editor_RegSetValue(HWND_VIEW, EEREG_EMEDITORPLUGIN, SZ_CONFIG, SZ_VALUE_SHORTCUT_KEY_STRING, REG_SZ, (const BYTE *)szShortcutKeyString, MAX_SIZE_OF_SHORTCUT_KEY_STRING * sizeof(WCHAR), 0);
      }

      GetDlgItemText(hDlg, IDC_EDIT3, szWaitForStartmSec, MAX_SIZE_OF_WAIT_FOR_START_MSEC);
      if(LPWSTR2DWORD(szWaitForStartmSec, dwWaitForStartmSec)){
        Editor_RegSetValue(HWND_VIEW, EEREG_EMEDITORPLUGIN, SZ_CONFIG, SZ_VALUE_WAIT_FOR_START_MSEC, REG_DWORD, (const BYTE *)&dwWaitForStartmSec, sizeof(DWORD), 0);
      }

      EndDialog(hDlg, 0);
      return FALSE;
    case IDCANCEL:
      EndDialog(hDlg, 0);
      return FALSE;
    default:
      return FALSE;
    }
  default:
  	return FALSE;
	}
}

/// <summary>
/// wgnuplot プロセスを管理するクラス GnuplotClass
/// </summary>
class GnuplotClass{
private:
  WCHAR szWgnuplotExecPath_[MAX_PATH];
  DWORD dwWaitForStartmSec_;

  STARTUPINFO si_;
  PROCESS_INFORMATION pi_;
  HWND hwndParent_, hwndText_;

  void getGnuplotWindowHandles(){
    HWND tmpHWNDArray[2];
    EnumThreadWindows(pi_.dwThreadId, callbackGetGnuplotHWND, (LPARAM)(&tmpHWNDArray));
    hwndParent_ = tmpHWNDArray[0];
    hwndText_   = tmpHWNDArray[1];
  }

  void postCommand(const WCHAR * c){
    while(*c) PostMessage(hwndText_, WM_CHAR, static_cast< unsigned int >(*c++), 1L);
  }
public:
  GnuplotClass(){}

  void setWgnuplotExecPath(WCHAR * szWgnuplotExecPath){ wcscpy_s(szWgnuplotExecPath_, MAX_PATH, szWgnuplotExecPath); }
  void setWaitForStartmSec(DWORD dwWaitForStartmSec  ){ dwWaitForStartmSec_ = dwWaitForStartmSec; }

  bool init(){
    ZeroMemory(&si_, sizeof(STARTUPINFO));
    si_.cb          = sizeof(STARTUPINFO);
    si_.dwFlags     = STARTF_USESHOWWINDOW;
    si_.wShowWindow = SW_SHOWMINIMIZED;
    ZeroMemory(&pi_, sizeof(PROCESS_INFORMATION));

    if(CreateProcess(NULL, szWgnuplotExecPath_, NULL, NULL, FALSE, 0, NULL, NULL, &si_, &pi_) == FALSE){ return false; }

    Sleep(dwWaitForStartmSec_); // 起動を待つ
    WaitForInputIdle(pi_.hProcess, INFINITE);

    getGnuplotWindowHandles();

    return true;
  }


  GnuplotClass & operator<<(const WCHAR * c){
    postCommand(c);
    return *this;
  }

  void cd(const WCHAR * dir){
    *this << L"\ncd '" << dir << L"'\n";
  }

  void exit(){
    *this << L"\nexit\n";
    CloseHandle(pi_.hProcess);
    CloseHandle(pi_.hThread);
  }

  void putOnTop(){
    SetWindowPos(hwndParent_, HWND_TOP, 0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE);
  }

  bool isProcessAlive(){
    DWORD code = 0;
    GetExitCodeProcess(pi_.hProcess, &code);
    return (code == STILL_ACTIVE);
  }
};



class CMyFrame : public CETLFrame<CMyFrame>
{
private:
  std::map< HANDLE, GnuplotClass > mapH2G;

  static const size_t MAX_LINE_LENGTH = 5000;

  // 全ての gnuplot プロセスを終了させる
  BOOL terminateAllProcesses(){
    std::map< HANDLE, GnuplotClass >::iterator it = mapH2G.begin();
    while(it != mapH2G.end()){
      GnuplotClass gc = (*it++).second;
      gc.exit();
    }

    return TRUE;
  }


public:
	// _loc.dll in MUI sub folder?
	enum { _USE_LOC_DLL			= FALSE					};

	// string ID
	enum { _IDS_MENU			= IDS_MENU_TEXT			};   // name of command, menu
	enum { _IDS_STATUS	  = IDS_MENU_TEXT			};   // description of command, status bar
	enum { _IDS_NAME			= IDS_MENU_TEXT			};   // name of plug-in, plug-in settings dialog box
	enum { _IDS_VER				= IDS_VERSION			  };   // version string of plug-in, plug-in settings dialog box

	// bitmaps
	enum { _IDB_BITMAP			= IDB_BITMAP			};
	enum { _IDB_16C_24			= 0						};
	enum { _IDB_256C_16_DEFAULT = 0						};
	enum { _IDB_256C_16_HOT		= 0						};
	enum { _IDB_256C_16_BW		= 0						};
	enum { _IDB_256C_24_DEFAULT = 0						};
	enum { _IDB_256C_24_HOT		= 0						};
	enum { _IDB_256C_24_BW		= 0						};
	enum { _IDB_TRUE_16_DEFAULT = 0						};
	enum { _IDB_TRUE_16_HOT		= 0						};
	enum { _IDB_TRUE_16_BW		= 0						};
	enum { _IDB_TRUE_24_DEFAULT = 0						};
	enum { _IDB_TRUE_24_HOT		= 0						};
	enum { _IDB_TRUE_24_BW		= 0						};

	// masks
	enum { _MASK_TRUE_COLOR		= RGB( 192, 192, 192 )	};
	enum { _MASK_256_COLOR		= RGB( 192, 192, 192 )	};

	// whether to allow a file is opened in the same window group during the plug-in execution.
	enum { _ALLOW_OPEN_SAME_GROUP = TRUE				};

	// whether to allow multiple instances.
	enum { _ALLOW_MULTIPLE_INSTANCES = TRUE				};

	// supporting EmEditor newest version * 1000
	enum { _MAX_EE_VERSION		= 11000					};

	// supporting EmEditor oldest version * 1000
	enum { _MIN_EE_VERSION		= 10000					};

	// supports EmEditor Professional
	enum { _SUPPORT_EE_PRO		= TRUE					};

	// supports EmEditor Standard
	enum { _SUPPORT_EE_STD		= TRUE					};

	// Queries whether the auto complete for brackets/quotation marks feature should be disabled.
	BOOL DisableAutoComplete( HWND /* hwnd */ )
	{
		return FALSE;
	}

	// Queries whether the plug-in wants to use dropped files.
	BOOL UseDroppedFiles( HWND /* hwnd */ )
	{
		return FALSE;
	}

	// The plug-in has been selected from a menu or a toolbar. 
	void OnCommand( HWND hwndView )
	{
    // EmEditor のハンドルをグローバルに確保
    HWND_VIEW = hwndView;

    // アクティブな（現在開いている）文書のハンドル
    HANDLE hActiveDoc = reinterpret_cast< HANDLE >(Editor_Info(hwndView, EI_GET_ACTIVE_DOC, NULL));
    
    GnuplotClass gc;
    if(mapH2G.count(hActiveDoc) == 0){  // 文書のハンドルが登録されていない
      WCHAR szWgnuplotExecPath[MAX_PATH];
      loadWgnuplotExecPath(hwndView, szWgnuplotExecPath);
      gc.setWgnuplotExecPath(szWgnuplotExecPath);

      DWORD dwWaitForStartmSec;
      loadWaitForStartmSec(hwndView, &dwWaitForStartmSec);
      gc.setWaitForStartmSec(dwWaitForStartmSec);

      // 初期化（wgnuplot プロセスを起動）
      if(!gc.init()){ Editor_OutputString(hwndView, L"Can't exec wgnuplot!\n", true); }

      // map に登録
      mapH2G[hActiveDoc] = gc;
    }else{  // 登録されている
      gc = mapH2G[hActiveDoc];  // 既存の GnuplotClass を利用
    }

    // wgnuplot が閉じていたら，再起動
    if(!gc.isProcessAlive()){
      gc.exit();
      gc.init();
      mapH2G[hActiveDoc] = gc;
    }

    // アクティブな文書のフルパス
    WCHAR szFullPath[MAX_PATH];
    Editor_Info(hwndView, EI_GET_FILE_NAMEW, (LPARAM)szFullPath);

    // 一時文書でない（フルパスが空でない）場合，存在するディレクトリを gnuplot の（デフォルトの）カレントディレクトリにする
    if(wcslen(szFullPath) != 0){
      WCHAR szDir[MAX_PATH];
      getDirectoryName(szFullPath, szDir, _countof(szDir));
      gc.cd(szDir);
    }

    // 行数
    UINT_PTR lineNum = Editor_GetLines(hwndView, POS_LOGICAL_W);

    // 1 行ずつ読み込み，wgnuplot に送る
		WCHAR szText[MAX_LINE_LENGTH];
		GET_LINE_INFO gli;
		gli.flags = FLAG_LOGICAL;
		gli.cch   = _countof(szText);
    for(UINT_PTR l = 0; l < lineNum; ++l){
      gli.yLine = l;
      szText[0] = 0;
      Editor_GetLineW(hwndView, &gli, szText);
      gc << szText << L"\n";
    }
    gc << L"\n";

    gc.putOnTop();
  }

	// Queries the status of the plug-in, whether the command is enabled and whether the plug-in is a checked status.
	BOOL QueryStatus( HWND /*hwndView*/, LPBOOL pbChecked )
	{		
		*pbChecked = FALSE;
		return TRUE;
	}

	// When a status is changed, this function is called with the Events parameter.
	void OnEvents( HWND hwndView, UINT nEvent, LPARAM lParam )
	{
    HWND_VIEW = hwndView;
    if(nEvent & EVENT_CREATE_FRAME){
      WCHAR szShortcutKeyString[MAX_SIZE_OF_SHORTCUT_KEY_STRING];
      loadShortcutKeyString(hwndView, szShortcutKeyString);
      setShortcutKey(szShortcutKeyString);
    }else
    if(nEvent & EVENT_DOC_CLOSE){
      HANDLE hClosingDoc = reinterpret_cast< HANDLE >(lParam);
      GnuplotClass gc = mapH2G[hClosingDoc];
      gc.exit();
      mapH2G.erase(hClosingDoc);
    }
	}

	// Queries whether the plug-in can be uninstalled.
	BOOL QueryUninstall( HWND /*hDlg*/ )
	{
		return TRUE;
	}

	// Uninstalls the plug-in.
	BOOL SetUninstall( HWND hDlg, LPTSTR pszUninstallCommand, LPTSTR pszUninstallParam )
	{
		TCHAR sz[80];
		TCHAR szAppName[80];
		LoadString( EEGetLocaleInstanceHandle(), IDS_SURE_TO_UNINSTALL, sz, sizeof( sz ) / sizeof( TCHAR ) );
		LoadString( EEGetLocaleInstanceHandle(), IDS_MENU_TEXT, szAppName, sizeof( szAppName ) / sizeof( TCHAR ) );
		if( MessageBox( hDlg, sz, szAppName, MB_YESNO | MB_ICONEXCLAMATION ) == IDYES ){

      // 保存していたデータを削除
      Editor_RegSetValue(HWND_VIEW, EEREG_EMEDITORPLUGIN, SZ_CONFIG, NULL, REG_SZ, NULL, 0, 0);

			return UNINSTALL_SIMPLE_DELETE;
		}
		return UNINSTALL_FALSE;
	}
	
	// Returns TRUE if Properties (dialog box) are supported.
	BOOL QueryProperties( HWND /*hDlg*/ )
	{
		return TRUE;
	}

	// Requests the plug-in to display the properties.
	BOOL SetProperties( HWND hDlg )
	{
    DialogBox(EEGetLocaleInstanceHandle() , MAKEINTRESOURCE( IDD_PROP ), hDlg, DlgProc );
    return FALSE;
	}

	// Called before each Windows message is translated.
	BOOL PreTranslateMessage( HWND hwndView, MSG* pMsg )
	{
    if(hwndView != NULL && hwndView == GetFocus()){
      if(pMsg->message == WM_KEYDOWN){
        if(pMsg->wParam == wcShortcutKey){
          if(iShortcutKyeFlags == getShortcutKeyFlagsFromKeyState()) OnCommand(hwndView);
        }
      }
    }
		return FALSE;
	}

	CMyFrame()
	{
	}

	~CMyFrame()
	{
    terminateAllProcesses();
	}

};



// the following line is needed after CMyFrame definition
_ETL_IMPLEMENT

