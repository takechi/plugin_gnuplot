//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by gnuplot.rc
//
#define IDS_MENU_TEXT                   100
#define IDB_BITMAP                      101
#define IDS_VERSION                     101
#define IDS_SURE_TO_UNINSTALL           102
#define IDD_PROP                        107
#define IDC_EDIT1                       1002
#define IDC_EDIT2                       1003
#define IDC_EDIT3                       1004
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
